
// outsource dependencies

// local dependencies


import welcome from './welcome/reducer';
import layout from './layout/reducer';
import users from './users/reducers';

export default {
    welcome,
    layout,
    users,
};
